package com.intothebasket.camel

import com.intothebasket.camel.listner.CallBack
import com.intothebasket.camel.routes.ProducerConsumerRoute
import org.apache.camel.main.Main
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class ProducerConsumerBootStrap {
    static final Logger logger = LoggerFactory.getLogger ProducerConsumerBootStrap.class


    static void main(String[] args) {
        Main main = new Main()

        //Add Camel Route
        main.configure().addRoutesBuilder(new ProducerConsumerRoute())

        //Add Consumer and Producers on initialization
        main.addMainListener(new CallBack())
        main.run()

        //Closing
        logger.info "Stopping Thread"
        main.stop()
    }
}
