package com.intothebasket.camel.routes

import org.apache.camel.builder.endpoint.EndpointRouteBuilder

class ProducerConsumerRoute extends EndpointRouteBuilder {

    //Simple Route to Transform Messages to Reverse

    @Override
    void configure() throws Exception {
        from(direct("start"))
                .routeId("TransformationRoute")
                .transform({ e, c ->
                    def msg = e.getMessage().getBody(String.class)
                    log.info "Transforming: $msg"
                    msg.reverse()
                })
                .to(seda("end"))
    }

}
