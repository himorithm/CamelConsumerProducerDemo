package com.intothebasket.camel.listner

import org.apache.camel.CamelContext
import org.apache.camel.main.BaseMainSupport
import org.apache.camel.main.MainListener
import org.slf4j.Logger
import org.slf4j.LoggerFactory

class CallBack implements MainListener {
    static final Logger logger = LoggerFactory.getLogger CallBack.class

    @Override
    void beforeInitialize(BaseMainSupport main) {

    }

    @Override
    void beforeConfigure(BaseMainSupport main) {

    }

    @Override
    void afterConfigure(BaseMainSupport main) {

    }

    @Override
    void configure(CamelContext context) {

    }

    @Override
    void beforeStart(BaseMainSupport main) {

    }

    //This method will get call only after Route is started.
    @Override
    void afterStart(BaseMainSupport main) {
        //Send Message to Transformation Route
        def template = main.getCamelContext().createFluentProducerTemplate()
        def message = "*** I did, did I ***"
        logger.info "Sent  $message"
        template.to("direct:start").withBody(message).send()

        //Receive Message From Transformation Route
        def consumerTemplate = main.getCamelContext().createConsumerTemplate()
        logger.info "Received: ${consumerTemplate.receiveBody("seda:end", String.class)}"

        main.getCamelContext().stop()
    }

    @Override
    void beforeStop(BaseMainSupport main) {

    }

    @Override
    void afterStop(BaseMainSupport main) {

    }
}
